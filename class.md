### classes[11] = "Python 201"

#### Análise de Sequências Biológicas 2023-2024

![Logo EST](assets/logo-ESTB.png)

Francisco Pina Martins

---

### Summary

* The focus of this class is advanced python programming
* We will focus on:
    * List comprehensions
    * Lambda functions
    * Recursion
    * Unit tests

---

### List comprehensions

* &shy;<!-- .element: class="fragment" -->A very *pythonic* way of performing iterations
* &shy;<!-- .element: class="fragment" -->But how do they work?
    * &shy;<!-- .element: class="fragment" -->Fully contained in `[]`
    * &shy;<!-- .element: class="fragment" -->An expression, followed by
        * &shy;<!-- .element: class="fragment" -->A `for` clause, followed by
            * &shy;<!-- .element: class="fragment" -->Zero or more `for` and `if` clauses
    * &shy;<!-- .element: class="fragment" -->`[expr for item in iter if condition]`

&shy;<!-- .element: class="fragment" -->![amazement](assets/deadpool.jpg)

|||

### List comprehensions

* &shy;<!-- .element: class="fragment" -->"Classic" *for loop* way

<div class="fragment">

```python
num_list = [1, 2, 3, 4, 5]

sqr_l = []

for i in num_list:
    i = i * i
    sqr_l.append(i)
```

</div>

* &shy;<!-- .element: class="fragment" -->*List comprehension* way

<div class="fragment">

```python
num_list = [1, 2, 3, 4, 5]

sqr_l = [i * i for i in num_list]
```

</div>

|||

### List comprehensions

* &shy;<!-- .element: class="fragment" -->Also great for edge cases like this:

<div class="fragment">

```python
text = "What's the answer to life, the universe & everything?"

answer = len([x for x in text if x not in (" ", ",", "'", "?")])
```

</div>

|||

### List comprehensions

* &shy;<!-- .element: class="fragment" -->The "expression" part can also be a function

<div class="fragment">

```python
def triple(x):
    """
    Calculates the triple of any number.
    Takes any `int()` as argument
    Returns an `int()`
    """
    return x * 3

num_list = [1, 2, 3, 4, 5]
tri_l = [triple(x) for x in num_list]
```

</div>

---

### What about dictionaries?

* &shy;<!-- .element: class="fragment" -->Dictionary comprehensions can also be used!
    * &shy;<!-- .element: class="fragment" -->Relies on the same principle as the list comprehension, but uses a dictionary
    * &shy;<!-- .element: class="fragment" -->**Very** vast
    * &shy;<!-- .element: class="fragment" -->Very *Pythonic*
* &shy;<!-- .element: class="fragment" -->These can really save a lot of typing

|||

### Dictionary comprehension

```python
# Get the triple of each value of `my_dict`
my_dict = {"a": 1, "b": 2, "c": 3}

# Using a loop:
triple_dict = {}
for k, v in my_dict.items():
    triple_dict[k] = v * 3

# Using a dict comprehension:
triple_dict = {k: v * 3 for k, v in my_dict.items()}
```

---

### Lambda functions

* &shy;<!-- .element: class="fragment" -->AKA "Anonymous functions"
* &shy;<!-- .element: class="fragment" -->Defined by the `lambda` keyword
* &shy;<!-- .element: class="fragment" -->Consist of 2 parts:
    * &shy;<!-- .element: class="fragment" -->Arguments
    * &shy;<!-- .element: class="fragment" -->Expression
    * &shy;<!-- .element: class="fragment" -->`lambda arguments: expression`
* &shy;<!-- .element: class="fragment" -->Can have "`+`" arguments, but only one expression
* &shy;<!-- .element: class="fragment" -->Cannot contain any statements!
* &shy;<!-- .element: class="fragment" -->Frequently combined with `map()` or `filter()`

&shy;<!-- .element: class="fragment" -->![Anonymous](assets/anonymous.jpg)

|||

### Lambda functions

* &shy;<!-- .element: class="fragment" -->Example:

<div class="fragment">

```python
def triple(x):
    """
    Calculates the triple of any number.
    Takes any `int()` as argument
    Returns an `int()`
    """
    return x * 3

# Single argument

triple = lambda x: x * 3

# Multiple arguments

add = lambda x, y : x + y

add(2, 3)
```

</div>

* &shy;<!-- .element: class="fragment" -->This is fun, but not much better than a "full" function

|||

### Lambda functions

* &shy;<!-- .element: class="fragment" -->In some cases, it makes a lot of sense, though

<div class="fragment">

```python
# Combined with map()

num_list = [1, 2, 3, 4, 5]
tri_l = list(map(lambda x: 3 * x, num_list))

# Combined with filter()

odd_list = list(filter(lambda x: x % 2 != 0, num_list))
```

</div>

* &shy;<!-- .element: class="fragment" -->[`filter()` function](https://docs.python.org/3/library/functions.html#filter)
* &shy;<!-- .element: class="fragment" -->[`map()` function](https://docs.python.org/3/library/functions.html#map)

---

### Recursion

* &shy;<!-- .element: class="fragment" -->"To understand recursion, you must understand recursion."
* &shy;<!-- .element: class="fragment" -->Recursion is when a function calls itself

&shy;<!-- .element: class="fragment" -->![Recursive Spider-man images](assets/recursion.jpg)

|||

### Recursion

* &shy;<!-- .element: class="fragment" -->Each time the function calls itself it reflects a simpler version of the original problem
* &shy;<!-- .element: class="fragment" -->Think of it like an alternative to iteration
* &shy;<!-- .element: class="fragment" -->Requires a "base condition" to prevent infinite recursion!

<div class="fragment">

```python
def listSum(ls):
    # Base condition
    if not ls:
        return 0

    # First element + result of calling `listsum` with rest of the elements
    return ls[0] + listSum(ls[1:])


listSum([1, 3, 4, 5, 6])
```

</div>

|||

### Recursion

* &shy;<!-- .element: class="fragment" -->Can also be used for integer calculations!

<div class="fragment">

``` python
def factorial(x):
    # Base condition. Also deals with `0!`
    if x == 0 or x == 1:
        return 1

    # Call the function with x-1 and multiply by x
    else:
        return x * factorial(x-1)
  
print(factorial(5))
```

</div>

---

### Unit tests

* &shy;<!-- .element: class="fragment" -->Short code fragments that *test* if a code *unit* is behaving as intended
* &shy;<!-- .element: class="fragment" -->Ideally independent of each other
* &shy;<!-- .element: class="fragment" -->There are several frameworks for writing python tests
 * &shy;<!-- .element: class="fragment" -->[unittest](https://docs.python.org/3/library/unittest.html)
 * &shy;<!-- .element: class="fragment" -->[nose2](https://nose2.io/)
 * &shy;<!-- .element: class="fragment" -->[pytest](https://docs.pytest.org/en/latest/)
* &shy;<!-- .element: class="fragment" -->Unit testing minimizes bug introduction in the code
* &shy;<!-- .element: class="fragment" -->Is usually associated with a *Continuous integration* (CI) service

|||

### Unit test example

```python
# my_function.py
def square_number(number):
    """
    Squares a number.
    """
    square = number * number
    return square
```

|||

### Unit test example

```python
# test_my_function.py
import pytest
import my_function


def test_square_number():
    """
    Tests if square_number() really is squaring a number
    """
    known_inputs = [2, 4, 6]
    expected_outputs = [4, 16, 36]

    for i, j in zip(known_inputs, expected_outputs):
        assert my_function.square_number(i) == j
```

* [`zip()` function](https://docs.python.org/3/library/functions.html#zip)

---

### Homework!

##### Script

* Write a small *python* script that converts FASTA to *Leave* NEXUS
* It must take a FASTA file as input as a command line argument
* Write 3 functions:
    1. Takes FASTA file as input, returns a {name: sequence} dict
    2. Takes the previous dict as input, returns the NEXUS DATA header
    3. Takes the previous dict as input, returns the NEXUS MATRIX block
* NEXUS file is outputted to STDOUT
* Assume "N" and "-" as missing and gap characters respectively

##### Test

* Write a python script with 3 unit tests - one for each function of the previous script

|||

### Homework! (more rules)

* Delivery date: May 20 2024 23:59
* Delivery mode: Github/Gitlab/Sourcehut/whatever repository link + commit hash via email
* Documentation: README.md describing the repository + function docstrings
    * What each script does
    * How to run each script
    * Motivation
* Setup: Groups of 4

|||

### Homework! (extra, not graded)

* Write an extra function that takes *ngen* and *outgroup* as input, and returns a "MrBayes Block"
    * These value can be read from as command line arguments too
* Cap sequence names to 99 characters
* [Here](assets/examples.tar.xz) are an example FASTA file and the respective NEXUS (including a *Mrbayes* block)

---

### References

* [List comprehensions](https://web.archive.org/web/20190319205826/http://blog.cdleary.com/2010/04/efficiency-of-list-comprehensions/)
* [Lambda functions in python](https://medium.com/@happymishra66/lambda-map-and-filter-in-python-4935f248593)
* [Recursion](https://realpython.com/python-recursion/)
* [Unit tests in python](https://realpython.com/pytest-python-testing/)
* [Interactive python](http://www.pythontutor.com/visualize.html#mode=edit)

